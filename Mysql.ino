package application;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class activatesoftwarecontroller implements Initializable {
	@FXML
	public Label activatestatuslabel = new Label();

	@FXML
	public Button activatebutton = new Button();
	public Connection con;
	public java.sql.Statement stat;
	public ResultSet rSet;

	// Constructor
	/*
	 * Creating a environementoffice database,If the database is already exists
	 * , it won't create a new one.
	 */
	public activatesoftwarecontroller() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
			stat = con.createStatement();
			String query = "CREATE DATABASE environementoffice";
			stat.executeUpdate(query);
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
		}
		System.out.println("Activate Software Construction first");
	}

	@FXML
	public void actionbuttonactivatesoftware(ActionEvent event) {

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		activatestatuslabel.setText("Software Is Not Activated");

		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/environementoffice", "root", "");
			stat = con.createStatement();
			activatestatuslabel.setText("Software Is Activated !");
			String sql = "";
			//Creating a colimn for water -> Industry -> Data Input
			sql = "CREATE TABLE waterindustrydatainput" + " "+
                    "(id INT NOT NULL AUTO_INCREMENT, " +
                    " day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " industryname VARCHAR(30), " +
                    " industrytype VARCHAR(30), "+ 
                    " industrylocation VARCHAR(300), " + 
                    " watertype VARCHAR(30), " + 
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10), " +
                    " remarks VARCHAR(300), " +
                    " PRIMARY KEY (id) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Air -> Monitoring -> Data Input
			sql = "CREATE TABLE airmonitoringdatainput" + " "+
					"(id INT NOT NULL AUTO_INCREMENT, " +
					" day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " spm VARCHAR(10), " +
                    " sox VARCHAR(10), " +
                    " nox VARCHAR(10), " +
                    " cox VARCHAR(10), " +
                    " primary key(id) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Air -> Indsutry -> Data Input
			sql = "CREATE TABLE airindustrydatainput" + " "+
					"(id INT NOT NULL AUTO_INCREMENT, " +
					" day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " indsutryname VARCHAR(30), " +
                    " indsutrtype VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " spm VARCHAR(10), " +
                    " sox VARCHAR(10), " +
                    " nox VARCHAR(10), " +
                    " cox VARCHAR(10), " +
                    " remarks VARCHAR(300), " +
                    " primary key (id) " + 
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Sound -> Monitoring -> Data Input
			sql = "CREATE TABLE soundmonitoringdatainput" + " "+
					"(id INT NOT NULL AUTO_INCREMENT, " +
					" day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " soundlevel VARCHAR(10), " +
                    " primary key (id)" + 
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Sound -> Indsutry -> Data Input
			sql = "CREATE TABLE soundindustrydatainput" + " "+
					"(id INT NOT NULL AUTO_INCREMENT, " +
					" day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " indsutryname VARCHAR(30), " +
                    " indsutrtype VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " soundlevel VARCHAR(10), " +
                    " remarks VARCHAR(300), " +
                    " primary key (id)" + 
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for water -> Industry -> Data Input
			sql = "CREATE TABLE waterindustrydatainput" + " "+
                    "(id INT NOT NULL AUTO_INCREMENT, " +
                    " day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " industryname VARCHAR(30), " +
                    " industrytype VARCHAR(30), "+ 
                    " industrylocation VARCHAR(300), " + 
                    " watertype VARCHAR(30), " + 
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10), " +
                    " remarks VARCHAR(300), " +
                    " PRIMARY KEY (id) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for water -> Industry -> Report Generate
			sql = "CREATE TABLE waterindustryreport" + " "+
                    "(date VARCHAR(10), " +
                    " industryname VARCHAR(30), " +
                    " industrytype VARCHAR(30), "+ 
                    " industrylocation VARCHAR(300), " + 
                    " watertype VARCHAR(30), " + 
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10), " +
                    " remarks VARCHAR(300) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			
			//Creating a colimn for Water -> Industry -> Report Show
			sql = "CREATE TABLE waterindustryreportshow" + " "+
                    "(serial VARCHAR(10), " +
                    " date VARCHAR(10), " +
                    " name VARCHAR(30) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Sweage -> Indsutry -> Report
			sql = "CREATE TABLE sweageindustryreport" + " "+
                    "(date VARCHAR(10), " +
                    " indsutryname VARCHAR(30), " +
                    " indsutrtype VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " bod VARCHAR(10), " +
                    " pox VARCHAR(10), " +
                    " nox VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " temperature VARCHAR(10), " +
                    " coliform VARCHAR(10), " +
                    " remarks VARCHAR(300) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Sweage -> Monitoring -> Report
			sql = "CREATE TABLE sweagemonitoringreport" + " "+
                    "(date VARCHAR(10), " +
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " bod VARCHAR(10), " +
                    " pox VARCHAR(10), " +
                    " nox VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " temperature VARCHAR(10), " +
                    " coliform VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Sound -> Monitoring -> Report
			sql = "CREATE TABLE soundmonitoringreport" + " "+
                    "(date VARCHAR(10), " +
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " soundlevel VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);

			
			//Creating a colimn for Sound -> Indsutry -> Report
			sql = "CREATE TABLE soundindustryreport" + " "+
                    "(date VARCHAR(10), " +
                    " indsutryname VARCHAR(30), " +
                    " indsutrtype VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " soundlevel VARCHAR(10), " +
                    " remarks VARCHAR(300) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Air -> Monitoring -> Report
			sql = "CREATE TABLE airmonitoringreport" + " "+
                    "(date VARCHAR(10), " +
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " spm VARCHAR(10), " +
                    " sox VARCHAR(10), " +
                    " nox VARCHAR(10), " +
                    " cox VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for Air -> Indsutry -> Report
			sql = "CREATE TABLE airindustryreport" + " "+
                    "(date VARCHAR(10), " +
                    " indsutryname VARCHAR(30), " +
                    " indsutrtype VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " spm VARCHAR(10), " +
                    " sox VARCHAR(10), " +
                    " nox VARCHAR(10), " +
                    " cox VARCHAR(10), " +
                    " remarks VARCHAR(300) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for water -> Monitoring -> Report Generate -> Pond
			sql = "CREATE TABLE watermonitoringreportpond" + " "+
                    "(date VARCHAR(10), " +
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), "+ 
                    " location VARCHAR(300), " + 
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for water -> Monitoring -> Report Generate -> Sea
			sql = "CREATE TABLE watermonitoringreportsea" + " "+
                    "(date VARCHAR(10), " +
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), "+ 
                    " location VARCHAR(300), " + 
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			//Creating a colimn for water -> Monitoring -> Report Generate -> Deep Tuble
			sql = "CREATE TABLE watermonitoringreporttuble" + " "+
                    "(date VARCHAR(10), " +
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), "+ 
                    " location VARCHAR(300), " + 
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			//Creating a colimn for water -> Monitoring -> Report Generate -> River
			sql = "CREATE TABLE watermonitoringreportriver" + " "+
                    "(date VARCHAR(10), " +
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), "+ 
                    " location VARCHAR(300), " + 
                    " watertype VARCHAR(30), " + 
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);

			
			// creating a column for Sweage -> Indsutry -> registration
			sql = "CREATE TABLE sweageindustryregistration" + " " + 
					"(industrytype VARCHAR(30), "+ 
					" industryname VARCHAR(50), " + 
					" industrylocation VARCHAR(300) " + 
					")";
			stat.executeUpdate(sql);
			

			
			//Creating a colimn for Sweage -> Monitoring -> Data Input
			sql = "CREATE TABLE sweagemonitoringdatainput" + " "+
                    "(day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " name VARCHAR(30), " +
                    " type VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " bod VARCHAR(10), " +
                    " pox VARCHAR(10), " +
                    " nox VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " temperature VARCHAR(10), " +
                    " coliform VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			// creating a column for Sweage -> Monitoring -> Registration
			sql = "CREATE TABLE sweagemonitoringregistration" + " " + 
					"(type VARCHAR(30), "+ 
					" name VARCHAR(50), " + 
					" location VARCHAR(300) " + 
					")";
			stat.executeUpdate(sql);
			
			//Creating a colimn for Sweage -> Indsutry -> Data Input
			sql = "CREATE TABLE sweageindustrydatainput" + " "+
                    "(day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " indsutryname VARCHAR(30), " +
                    " indsutrtype VARCHAR(30), " +
                    " location VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " bod VARCHAR(10), " +
                    " pox VARCHAR(10), " +
                    " nox VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " temperature VARCHAR(10), " +
                    " coliform VARCHAR(10), " +
                    " remarks VARCHAR(300) " +
                    ")"; 
			stat.executeUpdate(sql);
			

			
			// creating a column for Air -> Monitoring -> Registration
			sql = "CREATE TABLE airmonitoringregistration" + " " + 
					"(type VARCHAR(30), "+ 
					" name VARCHAR(50), " + 
					" location VARCHAR(300) " + 
					")";
			stat.executeUpdate(sql);
			
			
			// creating a column for Air -> Indsutry -> registration
			sql = "CREATE TABLE airindustryregistration" + " " + 
					"(industrytype VARCHAR(30), "+ 
					" industryname VARCHAR(50), " + 
					" industrylocation VARCHAR(300) " + 
					")";
			stat.executeUpdate(sql);
			
			
			// creating a column for Sound -> Indsutry -> registration
			sql = "CREATE TABLE soundindustryregistration" + " " + 
					"(industrytype VARCHAR(30), "+ 
					" industryname VARCHAR(50), " + 
					" industrylocation VARCHAR(300) " + 
					")";
			stat.executeUpdate(sql);
			
			
			// creating a column for Sound -> Monitoring -> Registration
			sql = "CREATE TABLE soundmonitoringregistration" + " " + 
					"(type VARCHAR(30), "+ 
					" name VARCHAR(50), " + 
					" location VARCHAR(300) " + 
					")";
			stat.executeUpdate(sql);
			
			
			//Creating a colimn for water -> Monitoring -> River Data Input
			sql = "CREATE TABLE watermonitoringriverdatainput" + " "+
                    "(day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " rivername VARCHAR(30), " +
                    " riverwatertype VARCHAR(30), "+ 
                    " riverlocation VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			
			//Creating a colimn for water -> Monitoring -> Sea Data Input
			sql = "CREATE TABLE watermonitoringseadatainput" + " "+
                    "(day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " seaname VARCHAR(30), " + 
                    " sealocation VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			
			//Creating a colimn for water -> Monitoring -> Pond Data Input
			sql = "CREATE TABLE watermonitoringponddatainput" + " "+
                    "(day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " pondname VARCHAR(30), " + 
                    " pondlocation VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			

			//Creating a colimn for water -> Monitoring -> Deep Tuble Data Input
			sql = "CREATE TABLE watermonitoringdeeptubledatainput" + " "+
                    "(day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " deeptublename VARCHAR(30), " + 
                    " deeptublelocation VARCHAR(300), " +  
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10) " +
                    ")"; 
			stat.executeUpdate(sql);			
			
			
			//Creating a coumn for ideal values
			String query = "CREATE TABLE IndustryIdealValues" + " " + 
					"(industrytype VARCHAR(30), "+ 
					" ph VARCHAR(10), " + 
					" bod VARCHAR(10), " + 
					" oilandgrease VARCHAR(10), " + 
					" totalhardness VARCHAR(10) " + 
					")";
			stat.executeUpdate(query);
			
			// creating a column for water -> Indsutry -> registration
			sql = "CREATE TABLE waterindustryregistration" + " " + 
					"(industrytype VARCHAR(30), "+ 
					" industryname VARCHAR(50), " + 
					" industrylocation VARCHAR(300) " + 
					")";
			stat.executeUpdate(sql);
			
			//Creating a colimn for water -> Industry -> Data Input
			sql = "CREATE TABLE waterindustrydatainput" + " "+
                    "(day VARCHAR(2), " +
                    " month VARCHAR(2), " + 
                    " year VARCHAR(4), " + 
                    " industryname VARCHAR(30), " +
                    " industrytype VARCHAR(30), "+ 
                    " industrylocation VARCHAR(300), " + 
                    " watertype VARCHAR(30), " + 
                    " labcode VARCHAR(10), " + 
                    " temperature VARCHAR(5), " +
                    " ph VARCHAR(5), " +
                    " ec VARCHAR(10), " + 
                    " ts VARCHAR(10), " +
                    " tds VARCHAR(10), " +
                    " ss VARCHAR(10), " +
                    " do VARCHAR(10), " + 
                    " cod VARCHAR(10), " +
                    " salinity VARCHAR(10), " +
                    " chloride VARCHAR(10), " +
                    " turbidity VARCHAR(10), " + 
                    " totalhardness VARCHAR(10), " +
                    " oilandgrease VARCHAR(10), " +
                    " bod VARCHAR(10), " +
                    " remarks VARCHAR(300) " +
                    ")"; 
			stat.executeUpdate(sql);
			
			// creating a column for water -> Monitoring -> Registration
			sql = "CREATE TABLE watermonitoringregistration" + " " + 
					"(type VARCHAR(30), "+ 
					" name VARCHAR(50), " + 
					" location VARCHAR(300) " + 
					")";
			stat.executeUpdate(sql);
			
			

		} catch (SQLException e) {
			System.out.println(e);
			activatestatuslabel.setText("Software Is Not Activated !");
		} catch (ClassNotFoundException e) {
			System.out.println(e);
			activatestatuslabel.setText("Software Is Not Activated !");
		}

	}

}
