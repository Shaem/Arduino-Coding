#include <Servo.h>
Servo myservo;  // create servo object to control a servo
int pos = 0;    // variable to store the servo position

int motorin1 = 7;
int motorin2 = 6;
int motorin3 = 5;
int motorin4 = 4;
int enableb = 10;
int enablea = 9;
int sonarsensorvcc=11;
#define trigPin A0
#define echoPin A1

float search(void)
{
  float duration = 0.00;                // Float type variable declaration
  float CM = 0.00;
  digitalWrite(trigPin, LOW);        // Trig_pin output as OV (Logic Low-Level)
  delayMicroseconds(2);              // Delay for 2 us
  digitalWrite(trigPin, HIGH);       // Trig_pin output as 5V (Logic High-Level)
  delay(100);
  digitalWrite(trigPin, LOW);        // Trig_pin output as OV (Logic Low-Level)
  duration = pulseIn(echoPin, HIGH); // Start counting time, upto again "echoPin" back to Logical "High-Level" and puting the "time" into a variable called "duration"
  CM = (duration / 58.82); //Convert distance into CM.
  return CM;
}
void setup() {
  myservo.attach(8);

  pinMode(motorin1, OUTPUT);
  pinMode(motorin2, OUTPUT);
  pinMode(motorin3, OUTPUT);
  pinMode(motorin4, OUTPUT);
  pinMode(enablea, OUTPUT);
  pinMode(enableb, OUTPUT);
  pinMode(sonarsensorvcc, OUTPUT);
  digitalWrite(sonarsensorvcc, HIGH);
  Serial.begin(9600);

}

void loop() {

  float  distance = search(); //ultrasonic sensor
  Serial.println(distance);

  if (distance <= 30) {
    digitalWrite(enablea, LOW);
    digitalWrite(enableb, LOW);
  }
  if (Serial.available()) {

    String data = Serial.readStringUntil('\n');
    Serial.println(data);
    if (data == "1") {
      //left to right for servo
      for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
        myservo.write(pos);              // tell servo to go to position in variable 'pos'
        delay(15);                       // waits 15ms for the servo to reach the position
      }
    }
    if (data == "2") {
      //right to left for servo
      for (pos = 180; pos >= 0; pos--) {
        myservo.write(pos);              // tell servo to go to position in variable 'pos'
        delay(15);
      }
    }
    if (data == "3") {//motor forward

      digitalWrite(enablea, HIGH);
      digitalWrite(enableb, HIGH);
      digitalWrite(motorin1, HIGH);
      digitalWrite(motorin2, LOW);
      digitalWrite(motorin3, HIGH);
      digitalWrite(motorin4, LOW);
    }
    if (data == "*3#") {//motor forward
      digitalWrite(enablea, HIGH);
      digitalWrite(enableb, HIGH);
      digitalWrite(motorin1, HIGH);
      digitalWrite(motorin2, LOW);
      digitalWrite(motorin3, HIGH);
      digitalWrite(motorin4, LOW);
    }
    if (data == "4") {//motor backward
      digitalWrite(enablea, HIGH);
      digitalWrite(enableb, HIGH);
      digitalWrite(motorin1, LOW);
      digitalWrite(motorin2, HIGH);
      digitalWrite(motorin3, LOW);
      digitalWrite(motorin4, HIGH);
    }
    if (data == "*4#") {//motor backward
      digitalWrite(enablea, HIGH);
      digitalWrite(enableb, HIGH);
      digitalWrite(motorin1, LOW);
      digitalWrite(motorin2, HIGH);
      digitalWrite(motorin3, LOW);
      digitalWrite(motorin4, HIGH);
    }
    if (data == "5") {
      digitalWrite(enablea, HIGH);
      digitalWrite(enableb, HIGH);
      digitalWrite(motorin1, HIGH);
      digitalWrite(motorin2, LOW);
      digitalWrite(motorin3, LOW);
      digitalWrite(motorin4, LOW);
      delay(300);
    }
    if (data == "*5#" || data == "*right#") {
      digitalWrite(enablea, HIGH);
      digitalWrite(enableb, HIGH);
      digitalWrite(motorin1, HIGH);
      digitalWrite(motorin2, LOW);
      digitalWrite(motorin3, LOW);
      digitalWrite(motorin4, LOW);
      delay(300);
    }
    if (data == "6") {
      digitalWrite(enablea, HIGH);
      digitalWrite(enableb, HIGH);
      digitalWrite(motorin1, LOW);
      digitalWrite(motorin2, LOW);
      digitalWrite(motorin3, HIGH);
      digitalWrite(motorin4, LOW);
      delay(300);
    }
    if (data == "*6#" || data == "*left#") {
      digitalWrite(enablea, HIGH);
      digitalWrite(enableb, HIGH);
      digitalWrite(motorin1, LOW);
      digitalWrite(motorin2, LOW);
      digitalWrite(motorin3, HIGH);
      digitalWrite(motorin4, LOW);
      delay(300);
    }

  }
}
