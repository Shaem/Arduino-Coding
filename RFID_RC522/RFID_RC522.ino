#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN1 10
#define RST_PIN 9
MFRC522 mfrc1(SS_PIN1, RST_PIN);


void setup()
{
  pinMode(10, OUTPUT);

  Serial.begin(9600);
  SPI.begin();
  mfrc1.PCD_Init();
  Serial.println("Author : Towqur Ahmed Shaem\nInstitution : Electrical and Electronic Engineering,CU\nWork : Embedded Engineer, Stellar Technology\nWork : Software Developer at Department of Govt. Environement Office,Chittagong\nEmail : towqirahmedshaem@gmail.com\nSite : www.gitlab.com/Shaem\nMobile : +8801521401124\n");
  Serial.println( "Typical pin layout used:\n -----------------------------------------------------------------------------------------\n              MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino\n              Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro\n  Signal      Pin          Pin           Pin       Pin        Pin              Pin\n  -----------------------------------------------------------------------------------------\n  RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST\n  SPI SS      SDA(SS)      10            53        D10        10               10\n  SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16\n  SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14\n  SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15\n -----------------------------------------------------------------------------------------");
  Serial.println("\nREADY TO READ CARD.......");
  Serial.println();
}

void loop()
{

  if ( mfrc1.PICC_IsNewCardPresent()) {

    if (! mfrc1.PICC_ReadCardSerial()) {
      return;
    }
    String Stringdata = "";
    
    //Monitoring UID number on serial monitor
    for (byte i = 0; i < mfrc1.uid.size; i++) {
      Stringdata.concat(String(mfrc1.uid.uidByte[i] < 0x10 ? " 0" : " "));
      Stringdata.concat(String(mfrc1.uid.uidByte[i], HEX));
    }

    if (Stringdata.substring(1) != "") {
      Stringdata.toUpperCase();
      Serial.println(Stringdata);
    }

  }
  delay(200);
}

