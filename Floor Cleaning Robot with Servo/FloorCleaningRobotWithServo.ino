/*
 * Author : Towqir Ahmed Shaem
 * Department : Electrical and Electronic Engineering, Chittagong University
 * Mobile : 01521401124
 * Email : towqirahmedshaem@gmail.com
 */
#define trigPin  A1         // Assign PIN A0 as trigPin (Connect ARDUINO UNO "A0" pin with Ultrasonic Sonar Sensor "TRIG" Pin) 
#define echoPin  A2
#define ultrasonicvcc A0// Assign PIN A1 as echoPin (Connect ARDUINO UNO "A1" pin with Ultrasonic Sonar Sensor "ECHO" Pin) 

#define MotorA_IN1  7       // Assign PIN 3 as MotorA_IN1 (Connect ARDUINO UNO "4" pin with L293D Motor Driver "IN1" Pin) 
#define MotorA_IN2  6       // Assign PIN 4 as MotorA_IN2 (Connect ARDUINO UNO "3" pin with L293D Motor Driver "IN2" Pin)
#define MotorB_IN3  5       // Assign PIN 5 as MotorB_IN3 (Connect ARDUINO UNO "7" pin with L293D Motor Driver "IN3" Pin)
#define MotorB_IN4  4       // Assign PIN 6 as MotorB_IN4 (Connect ARDUINO UNO "8" pin with L293D Motor Driver "IN4" Pin)
#define MotorA_PWM  9       // Assign PIN 9 as MOTORA_PWM (Connect ARDUINO UNO "5" pin with L293D Motor Driver "ENA" Pin) 
#define MotorB_PWM  10      // Assign PIN 10 as MOTORB_PWM (Connect ARDUINO UNO "6" pin with L293D Motor Driver "ENB" Pin)

#include <Servo.h>
Servo myservo;  // create servo object to control a servo
int pos = 0;    // variable to store the servo position
#define servovcc 3
void setup()
{
 
  pinMode(MotorA_IN1, OUTPUT);       // Declare "MotorB_IN1" as "Output Pin".
  pinMode(MotorA_IN2, OUTPUT);       // Declare "MotorB_IN2" as "Output Pin".
  pinMode(MotorB_IN3, OUTPUT);       // Declare "MotorB_IN3" as "Output Pin".
  pinMode(MotorB_IN4, OUTPUT);       // Declare "MotorB_IN4" as "Output Pin".
  pinMode(MotorA_PWM, OUTPUT);       // Declare "MotorA_PWM" as "Output Pin".
  pinMode(MotorB_PWM, OUTPUT);       // Declare "MotorB_PWM" as "Output Pin".
  pinMode(trigPin, OUTPUT);          // Declare "trigPin" as "Output Pin".
  pinMode(echoPin, INPUT);           // Declare "echoPin" as "Input Pin".
  
  pinMode(ultrasonicvcc, OUTPUT);
  digitalWrite(ultrasonicvcc, HIGH);
  pinMode(servovcc,OUTPUT);
  digitalWrite(servovcc,HIGH);
  myservo.attach(11);  // attaches the servo on pin 11 to the servo object
  Serial.begin(9600);
  servo();
  delay(5000);
}



float search(void)
{
  float duration = 0.00;                // Float type variable declaration
  float CM = 0.00;
  digitalWrite(trigPin, LOW);        // Trig_pin output as OV (Logic Low-Level)
  delayMicroseconds(2);              // Delay for 2 us
  digitalWrite(trigPin, HIGH);       // Trig_pin output as 5V (Logic High-Level)
  delayMicroseconds(10);             // Delay for 10 us
  digitalWrite(trigPin, LOW);        // Trig_pin output as OV (Logic Low-Level)
  duration = pulseIn(echoPin, HIGH); // Start counting time, upto again "echoPin" back to Logical "High-Level" and puting the "time" into a variable called "duration"
  CM = (duration / 58.82); //Convert distance into CM.
  return CM;
}

void servo() {
  for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(5);                       // waits 15ms for the servo to reach the position
  }
  for (pos = 180; pos >= 0; pos--) {
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(5);
  }
}
void RobotForward()
{
  digitalWrite(MotorA_IN1, HIGH);
  digitalWrite(MotorA_IN2, LOW);
  digitalWrite(MotorB_IN3, HIGH);
  digitalWrite(MotorB_IN4, LOW);
}

void RobotBackward()
{
  digitalWrite(MotorA_IN1, LOW);
  digitalWrite(MotorA_IN2, HIGH);
  digitalWrite(MotorB_IN3, LOW);
  digitalWrite(MotorB_IN4, HIGH);
}

void RobotLeft()
{
  digitalWrite(MotorA_IN1, LOW);
  digitalWrite(MotorA_IN2, HIGH);
  digitalWrite(MotorB_IN3, HIGH);
  digitalWrite(MotorB_IN4, LOW);
}

void RobotRight()
{
  digitalWrite(MotorA_IN1, HIGH);
  digitalWrite(MotorA_IN2, LOW);
  digitalWrite(MotorB_IN3, LOW);
  digitalWrite(MotorB_IN4, HIGH);
}

void RobotStop()
{
  digitalWrite(MotorA_IN1, LOW);
  digitalWrite(MotorA_IN2, LOW);
  digitalWrite(MotorB_IN3, LOW);
  digitalWrite(MotorB_IN4, LOW);
}

void loop()
{
  float distance = 0.00;
  float RobotSpeed = 0.00;
  float RightDistance = 0.00;
  float LeftDistance = 0.00;
  // Measuring the distance in CM
  distance = search();
  Serial.print("Distance :");
  Serial.println(distance);
  if ((distance <= 20)) // If obstracle found in 40 CM.
  {
    RobotSpeed = 255; // Speed Down
    analogWrite(MotorA_PWM, RobotSpeed); // Update speed in MOTORA Output Terminal
    analogWrite(MotorB_PWM, RobotSpeed); // Update speed in MOTORB Output Terminal
    RobotStop(); //Robot Stop
    delay(10);
    RobotBackward(); //Robot Run Backward Direction
    delay(400);
    RobotStop(); //Robot Stop
    delay(10);
    RobotRight();
    delay(500);
    RightDistance = search();
    delay(10);
    RobotLeft();
    delay(900);
    LeftDistance = search();
    delay(10);
    if (LeftDistance >= RightDistance)
    {
      RobotForward();
    }
    else
    {
      RobotRight();
      delay(800);
      RobotStop();
      delay(10);
      RobotForward();
    }
  }
  else if ((distance > 30) && (distance <= 70))
  {
    RobotSpeed = 200; // Speed increase slightly
    analogWrite(MotorA_PWM, RobotSpeed); // Update speed in MOTORA Output Terminal
    analogWrite(MotorB_PWM, RobotSpeed); // Update speed in MOTORB Output Terminal
    RobotForward();
    servo();
  }
  else
  {
    RobotSpeed = 255; // Speed increase to full speed
    analogWrite(MotorA_PWM, RobotSpeed); // Update speed in MOTORA Output Terminal
    analogWrite(MotorB_PWM, RobotSpeed); // Update speed in MOTORB Output Terminal
    RobotForward(); //Robot Move to Forward Direction
    servo();
  }
}

